import psycopg2
import psycopg2.extras
import warnings


class OnsaDB:
    def __init__(self, dbname, user, host, password):
        self.conn = psycopg2.connect(database=dbname, user=user, host=host, password=password)

    def query(self, querystr):
        try:
            cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute(querystr)
            rows = cur.fetchall()
        except Exception as e:
            warnings.warn("Got exception with msg: %s, returning empty list" % str(e))
            return []
        return rows

    def port_name(self, source_network, source_port, source_label):
        result = ":".join([source_network, source_port])
        if source_label is not None:
            label = source_label[1:-1].split(",")
            return "%s?%s=%s" % (result, label[0], label[1])
        return "%s" % result


    def get_sub_connections(self, connid):
        rows = self.query("SELECT * from sub_connections  where service_connection_id = '%s' ORDER BY id" % connid)
        sub_connections = []
        for row in rows:
            sub_connections.append({
                "cid": row['connection_id'],
                "nsa": row['provider_nsa'],
                "rsm": row['reservation_state'],
                "psm": row['provision_state'],
                "lsm": row['lifecycle_state'],
                "dsm": row['data_plane_active'],
                "version": row['data_plane_version'],
                "source": self.port_name(row['source_network'], row['source_port'], row['source_label']),
                "dest": self.port_name(row['dest_network'], row['dest_port'], row['dest_label']),
            })

        return sub_connections


    def get_connections(self, where_clause):
        rows = self.query("SELECT * FROM service_connections %s ORDER BY end_time desc" % where_clause)
        connections = []
        for row in rows:
            sub_connections = self.get_sub_connections(row['id'])
            if len(sub_connections) < 1:
                continue
            connections.append({"start": row['start_time'],
                                "bandwidth": row['bandwidth'],
                                "cid": row['connection_id'],
                                "gid": row['global_reservation_id'],
                                "description": row['description'],
                                "rsm": row['reservation_state'],
                                "psm": row['provision_state'],
                                "lsm": row['lifecycle_state'],
                                "dsm": all([sc['dsm'] for sc in sub_connections]),
                                "end": row['end_time'],
                                "requester": row['requester_nsa'],
                                "version": row['revision'],
                                "source": self.port_name(row['source_network'], row['source_port'],
                                                         row['source_label']),
                                "dest": self.port_name(row['dest_network'], row['dest_port'], row['dest_label']),
                                "sub_connections": sub_connections,
            })
        return connections