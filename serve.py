#!/usr/bin/env python

from flask import Flask
from flask import render_template
from flask_themes2 import Themes, render_theme_template, get_theme, get_themes_list
#from flask_themes2 import *
from datetime import datetime
from config import *

from onsa_db import *


app = Flask(__name__)
Themes(app, app_identifier="nsilog")


def render(template, **context):
    theme=None
    try:
        theme=get_theme(THEME)
    except:
        theme=get_theme("default")
    return render_theme_template(theme, template, **context)


@app.route('/')
def usage():
    return render('index.jinja2')


@app.route('/connections')
def connections():
    now = datetime.utcnow()
    past_connections = db.get_connections("WHERE end_time < '%s' ::timestamp" % now)
    current_connections = db.get_connections(
        "WHERE start_time < '%s' ::timestamp AND end_time > '%s' ::timestamp" % (now, now))
    future_connections = db.get_connections("WHERE start_time > '%s' ::timestamp" % now)
    return render('connections.jinja2', a_random_string="Heey, what's up!",
                           current_connections=current_connections, past_connections=past_connections,
                           future_connections=future_connections)

if __name__ == '__main__':
    global db
    db = OnsaDB(DB_NAME, DB_USER, DB_HOST, DB_PASS)
    app.run(WEB_HOST, WEB_PORT, debug=WEB_DEBUG)
