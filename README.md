About
-----

onsa-lg is a looking glass for OpenNSA, it shows current past and future connections.

Installation
------------
    $ pip install psycopg2
    $ pip install flask
    $ pip install flask-themes2

Configuration
-------------

If required, create or modify the default theme, documentation can be found there at: 
http://flask-themes2.readthedocs.org/en/latest/

First add a read only user to access the opennsa database

    :::SQL
    postgres=# GRANT CONNECT ON opennsa to onsalg
    postgres=# ALTER ROLE onsalg LOGIN PASSWORD '***';
    
    opennsa=> GRANT SELECT ON service_connections TO onsalg;
    opennsa=> GRANT SELECT ON sub_connections TO onsalg ;
    opennsa=> GRANT SELECT ON generic_backend_connections TO onsalg ;


Create a file called config.py in the onsa-lg directory with the following content: 

    :::python
    WEB_HOST="nsa.uvalight.nl"
    WEB_PORT=5011
    WEB_DEBUG=True
    DB_NAME='opennsa'
    DB_USER='opennsa'
    DB_HOST='localhost'
    DB_PASS='thepassword'
    THEME='default'

Usage
-----

To start the looking glass.

  ./serve.py 

